package main

import (
	"fmt"
	"io/ioutil"
	"net"
	"strings"

	"github.com/cenkalti/rpc2"
)

var (
	myIP    string
	kdcName string
	kdcName string
)

type Args struct{ A, B int }
type Reply int

type ArgsUser struct{ uName string }
type TGT struct {
	username, clientIP, kdcName string
}
type ReplyString string

func main() {
	myIP := "192.168.0.11"

	srv := rpc2.NewServer()
	srv.Handle("add", func(client *rpc2.Client, args *Args, reply *Reply) error {

		// Reversed call (server to client)
		var rep Reply
		client.Call("mult", Args{2, 3}, &rep)
		fmt.Println("mult result:", rep)

		*reply = Reply(args.A + args.B)
		return nil
	})

	srv.Handle("checkUser", func(client *rpc2.Client, args *ArgsUser, replyString *ReplyString) error {

		// Reversed call (server to client)
		// var rep Reply
		// client.Call("mult", Args{2, 3}, &rep)
		// fmt.Println("mult result:", rep)

		// Check if user exists
		userData, err := ioutil.ReadFile("users.dat")
		check(err)
		userDetails := strings.Split(string(userData), " ")
		username := userDetails[0]
		userpassword := userDetails[1]

		if len(args.uName) > 0 {
			if username == args.uName {
				// Send back TGT
				client.State.Get("ip")

			}
		}

		//fmt.Println(string(userData))
		*replyString = ReplyString(string(userData))
		return nil
	})

	lis, _ := net.Listen("tcp", "127.0.0.1:5000")
	srv.Accept(lis)
}

func check(e error) {
	if e != nil {
		panic(e)
	}
}
