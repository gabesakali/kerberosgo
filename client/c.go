package main

import (
	"fmt"
	"log"
	"net"

	"github.com/cenkalti/rpc2"
)

var (
	myIP string
)

type Args struct{ A, B int }
type Reply int

type ArgsUser struct{ uName string }
type ReplyString string

func main() {
	conn, _ := net.Dial("tcp", "127.0.0.1:5000")

	clt := rpc2.NewClient(conn)
	clt.Handle("mult", func(client *rpc2.Client, args *Args, reply *Reply) error {
		*reply = Reply(args.A * args.B)
		return nil
	})
	go clt.Run()

	var rep Reply
	var repStr ReplyString
	var x, y int
	var userName string

	fmt.Println("Please enter two integers: x and y SPACED")
	_, err := fmt.Scanf("%d%d\n", &x, &y)

	if err != nil {
		log.Fatal(err)
	} else {
		// Continue
		fmt.Println(x, "and", y)

		clt.Call("add", Args{x, y}, &rep)
		fmt.Println("add result:", rep)

		// Check if self exists
		fmt.Println("Please enter your username")
		_, err := fmt.Scanf("%s\n", &userName)

		clt.Call("checkUser", ArgsUser{userName}, &repStr)
		fmt.Println("user search result:", repStr)
	}

}
